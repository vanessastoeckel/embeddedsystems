/** 
 * @file   HelloWorld3.c
 * @author Marina Rost, Jana Seemann, Vanessa St�ckel, Janina W�rz
 *
 * @date 07.12.2017 - 31.01.2018
 * 
 * @brief Getting started with a simple program (blinking LED)
 */



// Check for Project Settings
#ifndef __dsPIC33EP512MU810__
    #error "Wrong Controller"
#endif

/* ***********************
 * Includes
 * ***********************
 */

#include "myxc.h"
#include <stdint.h>     //Standard typedefs
#include <stdbool.h>
#include <stdio.h>
#include "digitalIO.h"
#include <stdlib.h>
#include <stddef.h>
#include <p33EP512MU810.h>
#include "lcd.h"

/* ***********************
 * Defines
 * ***********************
 */

/* Substitute for stdlib.h */
//#define	EXIT_SUCCESS	0
//#define	EXIT_FAILURE	1

/* Hardware  */
#define _LED200 LATBbits.LATB8
#define _LED201 LATBits.LATB8

#define SW200 4 //PG12
#define SW201 5
#define SW202 6
#define SW203 7 //PG15

#define HIGH 1
#define LOW 0

#define MIN_ROUNDS 1
#define MAX_ROUNDS 100
#define MIN_TIME 1
#define MAX_TIME 500

/* ***********************
 * Prototypes
 * ***********************
 */

/* ***********************
 * Define Variables
 * ***********************
 */
bool aktuellerSpielerA = false;
bool aktuellerSpielerB = false; 
uint16_t minA = 0;
uint16_t sekA = 0;
uint16_t minB = 0;
uint16_t sekB = 0;
char buffer [128];
uint16_t puffNumber = 10;
uint16_t roundA;
uint16_t roundB;
uint16_t time = 0;
bool gameOver = true;

/* ***********************
 * Function definitions
 * ***********************
 */
void delay_ms(uint16_t u16milliseconds);
void timersetzen(void);
void startScreen(void);
uint8_t digitalRead(uint16_t pin);
void initInterrupts(void);
void init(void);
void showStartScreen(void);
void waitForUnpressedEncoder(void);
void waitForPressedEncoder(void);
void settings(void);
void resetAfterGameOver(void);
void displayTime(void);
void switchToPlayerA(void);
void switchToPlayerB(void);

/* ***********************
 * Main
 * ***********************
 */


/**
 * Hello World (Blinking LED)
 * @brief Toggles LED200 with 1 Hz, 1:1 duty cycle
 * @pre CPU clock frequency = 8.00 MHz
 */

int main() {
    init();
    showStartScreen();
    
    while(1){
        if(gameOver == true){
            resetAfterGameOver();
        }
        settings();
        /* Endless Loop */
        while(!gameOver){   
            switchToPlayerA();
            switchToPlayerB();
            
            //Code wird pro sekunde ein mal aufgerufen
            if(IFS0bits.T1IF == 1){
                _LED200=!_LED200; //Toggle LED
                
                if(aktuellerSpielerA==true){
                    if(sekA == 0 && minA != 0){
                        minA--;
                        sekA=59;
                    } else{
                        sekA--;
                    }
                    if(minA == 0 && sekA == 0){
                        LCD_ClearScreen();
                        LCD_PutString("GameOver\r\nPlayer B won!", 25);  
                        gameOver = true;
                        while(digitalRead(INC_SW)) {}
                        while(!digitalRead(INC_SW)) {
                            delay_ms(100);
                        }                
                    } 
                    if(!digitalRead(INC_SW)) {
                        LCD_ClearScreen();
                        LCD_PutString("Player B \r\nSchachmatt", 21);
                        gameOver = true;
                        while(!digitalRead(INC_SW)) {}
                        while(digitalRead(INC_SW)) {
                            delay_ms(100);
                        }
                    }
                } 
                if(aktuellerSpielerB==true){
                    roundB--;
                    if(sekB == 0 && minB != 0){
                        minB--;
                        sekB=59;
                    } else {
                        sekB--;
                    } 
                    if(minB == 0 && sekB == 0){
                        LCD_ClearScreen();
                        LCD_PutString("GameOver\r\nPlayer B won!", 25);
                        gameOver = true;
                        while(digitalRead(INC_SW)) {}
                        while(!digitalRead(INC_SW)) {
                            delay_ms(100);
                        }
                    } 
                    if(!digitalRead(INC_SW)) {
                        LCD_ClearScreen();
                        LCD_PutString("Player A \r\nSchachmatt", 21);
                        gameOver = true;
                        while(!digitalRead(INC_SW)) {}
                        while(digitalRead(INC_SW)) {
                            delay_ms(100);
                        }
                    }
                }                  
                if(roundA < 1||roundB < 1) {
                    LCD_ClearScreen();
                    LCD_PutString("Zuege \r\naufgebraucht", 20);
                    gameOver = true;
                    waitForPressedEncoder();
                    waitForUnpressedEncoder();
                }
                IFS0bits.T1IF = 0;
                displayTime();
            }//ende if; code au�erhalb der Sekunde/�fter als 1x;

        }//while
    }
    return (EXIT_SUCCESS);  //never reached
} //main()
void initInterrupts(){
    /* Port Configurations */
    // DS70616G-page 209
    // ODCB (open drain config) unimplemented (DS70616G, Table 4-56)
    ANSELBbits.ANSB8=0;     //Digital I/O
    CNENBbits.CNIEB8=0;     //Disable change notification interrupt
    CNPUBbits.CNPUB8=0;     //Disable weak pullup
    CNPDBbits.CNPDB8=0;     //Disable weak pulldown
    TRISBbits.TRISB8=0;     //Pin B8: Digital Output
    LATBbits.LATB8=0;       //Pin B8: Low
}
void init(){
    initInterrupts();
    LCD_Initialize();
    initEncoder();
    timersetzen();
}
void showStartScreen(){
    LCD_ClearScreen();
    LCD_PutString("Willkommen bei  Blitzschach", 27); 
    waitForUnpressedEncoder();
    waitForPressedEncoder();
}
void waitForUnpressedEncoder(){
    while(digitalRead(INC_SW)) {
        delay_ms(100);
    }
}
void waitForPressedEncoder(){
    while(!digitalRead(INC_SW)) {
        //do nothing
        delay_ms(100);
    }
}
void resetAfterGameOver(){
    minA = 0;
    minB = 0;
    aktuellerSpielerA = false;
    aktuellerSpielerB = false;
    gameOver = false;
}
void settings(){
    while(digitalRead(INC_SW)) {
        LCD_ClearScreen();
        sprintf(buffer, "Zugzahl:\r\n%03d Zuege", puffNumber);
        LCD_PutString(buffer, 20);
        puffNumber += readEncoderPulse();
        if(puffNumber < MIN_ROUNDS) {
            puffNumber = MIN_ROUNDS;
        } else if(puffNumber > MAX_ROUNDS) {
            puffNumber = MAX_ROUNDS;
        }
    }
    roundA = puffNumber*2;
    roundB = puffNumber*2;
    waitForPressedEncoder();
    while(digitalRead(INC_SW)) {
        LCD_ClearScreen();
        sprintf(buffer, "Zeit: %03d min", time);
        LCD_PutString(buffer, 13);
        time += readEncoderPulse();
        if(time < MIN_TIME) {
            
            
            time = MIN_TIME;
        } else if(time > MAX_TIME) {
            time = MAX_TIME;
        }
    }
    minA = time;
    minB = time;
    waitForPressedEncoder();
}
void displayTime(){
    LCD_ClearScreen();
    //wandelt int in char um und gibt so die Spielzeit aus:
    sprintf (buffer, "%02d:%02d", minA, sekA);
    LCD_PutString("PlayerA: ",10);
    LCD_PutString(buffer,5);

    //LCD_setPosition(1, 0);
    sprintf (buffer, "%02d:%02d", minB, sekB);
    LCD_PutString("\r\nPlayerB: ",11);//,buffer
    LCD_PutString(buffer,5);//,buffer       
}
void switchToPlayerA(){
    if(!digitalRead(SW203)){
        if (!(aktuellerSpielerA&&aktuellerSpielerB)||aktuellerSpielerB){
            aktuellerSpielerA = true;
            aktuellerSpielerB = false;
            roundA--;
        }
    }
}
void switchToPlayerB(){
    if(!digitalRead(SW202)) {
        if (!(aktuellerSpielerA&&aktuellerSpielerB)||aktuellerSpielerA){
            aktuellerSpielerB = true;
            aktuellerSpielerA = false;
            roundB--;
        }
    }
}