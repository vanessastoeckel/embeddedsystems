/* 
 * File:   Timer.c
 * Author: Janina
 *
 * Created on 18. Dezember 2017, 14:11
 */

#include <xc.h>

#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "lcd.h"



/*
 * 
 */

    
    void timersetzen(void)
    {   
        T1CONbits.TON = 0; // Disable Timer
        T1CONbits.TCS = 0; // Select internal instruction cycle clock
        T1CONbits.TGATE = 0; // Disable Gated Timer mode
        T1CONbits.TCKPS = 0b011; // Select 1:256 Prescaler
        TMR1 = 0x00; // Clear timer register
        PR1 = 15625; // Load the period value
        T1CONbits.TON = 1; // Start Timer
    }
    
    void startScreen(void)
    {
        LCD_PutString("Welcome", 9);
    }
    
    
    
   
        
        
        
        
  


        