/** \mainpage Index Page
 *
 * \section genDescr_sec General Description
 *
 * The purpose of this software is getting started quickly with a basic functionality
 * (blinking LED).
 *
 * \section hardware_sec Hardware
 * The software has been designed for the EDA dsPIC33-board.
 * Documentation is available on https://eda-wiki.eda.htw-aalen.de/studi/PIClab/ExperimentierBoardPIC24dsPIC33
 *
 * \subsection hwSub_sec Hardware Variants
 * Although beeing designed for the dsPIC33 variant, the software should also run on a board populated
 * with the PIC24H-Controller. This has not been tested.
 */
