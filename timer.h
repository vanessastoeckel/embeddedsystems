/* 
 * File:   timer.h
 * Author: Jana
 *
 * Created on 22. Januar 2018, 10:43
 */

#ifndef TIMER_H
#define	TIMER_H

#ifdef	__cplusplus
extern "C" {
#endif


    void timerSetzen(void);

#ifdef	__cplusplus
}
#endif

#endif	/* TIMER_H */

